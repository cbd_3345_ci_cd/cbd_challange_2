FROM ubuntu:latest
LABEL authors="abeyp"

#package list and necessary dependencies
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    && rm -rf /var/lib/apt/lists/* \

# setting up enviornment variables \
ENV PYTHONDONTWRITEBYTECODE 1 \
ENV PYTHONUNBUFFERED 1

#setting the working dir
WORKDIR /app

#copying the current directory contents
COPY . /app

# INATALL DEPENDENCIES
RUN pip3 install --no-cache-dir -r requirements.txt

#expose port 7001
EXPOSE 7001

#defining command t run application
CMD ["python3","main.py"]