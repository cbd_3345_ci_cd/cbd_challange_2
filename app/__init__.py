import os

from flask import Flask
from flask_session import Session
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = os.getenv("SESSION_PERMANENT")
app.config["SESSION_TYPE"] = os.getenv("SESSION_TYPE")
Session(app)

# Dummy user and product data
users = {"user1": os.getenv("USER_PASSWORD")}

products = {
    1: {"name": "Laptop", "price": 1000},
    2: {"name": "Mouse", "price": 50},
    3: {"name": "Keyboard", "price": 100}
}

from app import routes
